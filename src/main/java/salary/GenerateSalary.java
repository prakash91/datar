package salary;

import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import launch.Login;
import properties.CaptureOutput;
import properties.HandleLogs;
import properties.LoadProperties;

public class GenerateSalary extends LoadProperties {

	WebDriver dr;
	String getToasterMsg;
	CaptureOutput co = new CaptureOutput();
	HandleLogs log = new HandleLogs();
	Login lgn = new Login();

	@BeforeMethod
	public void beforeMethod() {
		lgn.login();
		dr = lgn.getLoginDriverInstance();
	}

	@Test(priority=0)
	public void generateSalaryForEmployee() throws InterruptedException, IOException {

		String testCase = "Generate Salary For Employee ---> ";

		Actions action = new Actions(dr);

		log.printLogsToFile("**********"+testCase+"Started **********"+ new Date());

		//Click on HR & Payroll menu
		hrPayrollMenu.click();

		//Click on Generate Salary link
		generateSalary.click();

		//Click on Select Report type drop down
		rTypeDrop.click();

		//Select Single Employee Option from the Drop Down
		option1.click();

		Thread.sleep(1000);

		//Enter Employee
		lgn.locateElementByXPath("//*[@id=\'row\']/div[1]/input", LoadProperties.gSEmp);

		Thread.sleep(1000);

		//Select Employee
		WebElement emp = dr.findElement(By.xpath("//*[@id=\'row\']/div[1]/div/button"));
		action.moveToElement(emp);
		action.sendKeys(Keys.ENTER);
		action.perform();

		Thread.sleep(1000);

		//Select Month & Year
		lgn.locateElementByName("monthandYear", LoadProperties.gSEMonth);

		//Select Year
		WebElement selectMonth = dr.findElement(By.name("monthandYear"));
		action.moveToElement(selectMonth);
		action.sendKeys(Keys.ARROW_RIGHT);
		action.sendKeys(LoadProperties.gSEYear);
		action.perform();

		//Enter Total Working Days
		totalWorkDays.sendKeys(LoadProperties.gSETotalWorkDays);

		//Click on Generate Button
		genSalary.click();
		
		Thread.sleep(1000);

		getToasterMsg = toasterMsg.getText();
		

		if(getToasterMsg.contains("There are no duties found for user :")) {
			log.printLogsToFile(testCase +getToasterMsg);
			log.printLogsToFile(testCase + "Generate Salary for employee failed");
		}else {
			log.printLogsToFile(testCase +getToasterMsg);
			log.printLogsToFile(testCase + "Generate Salary for employee Passed");
		}
	}

	@Test(priority=1)
	public void generateSalaryForClient() throws InterruptedException, IOException {

		String testCase = "Generate Salary For Client ---> ";
		Actions action = new Actions(dr);

		log.printLogsToFile("**********"+testCase+"Started **********"+ new Date());

		//Click on HR & Payroll menu
		hrPayrollMenu.click();

		//Click on Generate Salary link
		generateSalary.click();

		//Click on Select Report type drop down
		rTypeDrop.click();

		//Select Client Option from the Drop Down
		option2.click();

		Thread.sleep(1000);

		//Enter Client
		lgn.locateElementByXPath("//*[@id=\'react-select-3--value\']", LoadProperties.gSClient);

		Thread.sleep(1000);

		//Select Client
		WebElement clnt = dr.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/div[2]/form/div[1]/div[2]/div[1]/div/div[1]/div[2]"));
		action.moveToElement(clnt);
		action.sendKeys(Keys.TAB);
		action.perform();

		Thread.sleep(1000);

		//Select Month
		lgn.locateElementByName("monthandYear", "jan");

		//Select Year
		WebElement selectMonth = dr.findElement(By.name("monthandYear"));
		action.moveToElement(selectMonth);
		action.sendKeys(Keys.ARROW_RIGHT);
		action.sendKeys(LoadProperties.gSYear);
		action.perform();

		//Enter Total Working Days
		totalWorkDays.sendKeys(LoadProperties.gSTotalWorkDays);

		//Click on Generate Button
		genSalary.click();
		Thread.sleep(1000);
		
		getToasterMsg = toasterMsg.getText();

		dr.quit();
		if(getToasterMsg.contains("There are no duties found")) {
			log.printLogsToFile(testCase +getToasterMsg);
			log.printLogsToFile(testCase + "Generate Salary for employee failed");
		}else {
			log.printLogsToFile(testCase +getToasterMsg);
			log.printLogsToFile(testCase + "Generate Salary for employee Passed");
		}
	}

	@AfterMethod
	public void afterMethod(){
		dr.quit();
	}
}
