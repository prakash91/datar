package tenders;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import launch.Login;
import properties.HandleLogs;
import properties.LoadProperties;

public class CreateTender extends LoadProperties {

	WebDriver dr;
	Login lgn = new Login();
	HandleLogs logs = new HandleLogs();
	
	@BeforeMethod
	public void beforeMethod() {
		lgn.login();
		dr = lgn.getLoginDriverInstance();
	}
	@Test
	public void createParent() throws InterruptedException {

		String warning = null;
		String success;
		String testCase = "Parent Tender Creation ---> ";

		Actions action = new Actions(dr);

		//Click on Tender Management menu
		tenderMenu.click();

		//Click on create tender button
		create.click();

		//Click on parent radio button
		parentRadio.click();

		//Enter Parent description
		description.sendKeys(LoadProperties.pTender);

		//Select due date
		date.sendKeys(LoadProperties.pTenDueDate);

		//Enter Assignee
		lgn.locateElementByXPath("//*[@id='react-select-3--value']",LoadProperties.pTenAssignee);

		Thread.sleep(1000);

		//Select Assignee
		WebElement assignee = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[1]/div[1]/div[3]/div[2]/div/div[1]/div[2]"));
		action.moveToElement(assignee);
		action.sendKeys(Keys.TAB);
		action.perform();

		//Enter Expected Hours
		expectedHours.sendKeys(LoadProperties.pTenExpHours);

		//Enter Interested Party
		lgn.locateElementByXPath("//*[@id=\'react-select-4--value\']", LoadProperties.pTenIntParty);

		Thread.sleep(1000);

		//Select Interested Party
		WebElement intParty = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[1]/div[1]/div[4]/div[2]/div/div[1]/div[2]"));
		action.moveToElement(intParty);
		action.sendKeys(Keys.TAB);
		action.perform();

		Thread.sleep(1000);

		//Select Severty Dropdown
		sevDrop.click();

		//Select Severty
		option2.click();

		JavascriptExecutor jse = (JavascriptExecutor)dr;
		jse.executeScript("scroll(0, 700)");

		Thread.sleep(1000);

		// Click on Save
		save.click();

		Thread.sleep(1000);

		try {
			success = toastMsg.getText();
			logs.printLogsToFile(testCase+ success);
		}catch(Exception e) {
			warning = warn.getText();
			logs.printLogsToFile(testCase+ warning);
		}
	}
	@Test(priority=1)
	public void createChild() throws InterruptedException {

		String warning = null;
		String success;
		String testCase = "Child Tender Creation ---> ";

		Actions action = new Actions(dr);

		//Click on Tender management menu
		tenderMenu.click();

		//Click on create tender button
		create.click();

		//Enter parent tender 
		lgn.locateElementByXPath("//*[@id=\'react-select-2--value\']", LoadProperties.pTender);

		Thread.sleep(2000);

		//Select Parent tender
		WebElement parent = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[1]/div[1]/div[1]/div/div/div[1]/div[2]"));
		action.moveToElement(parent);
		action.sendKeys(Keys.TAB);
		action.perform();

		//Enter Child description
		description.sendKeys(LoadProperties.cTender);

		//Select due date
		date.sendKeys(LoadProperties.cTenDueDate);

		//Enter Assignee
		lgn.locateElementByXPath("//*[@id='react-select-3--value']",LoadProperties.cTenAssignee);

		Thread.sleep(1000);

		//Select Assignee
		WebElement assignee = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[1]/div[1]/div[3]/div[2]/div/div[1]/div[2]"));
		action.moveToElement(assignee);
		action.sendKeys(Keys.TAB);
		action.perform();

		//Enter Expected Hours
		expectedHours.sendKeys(LoadProperties.cTenExpHours);

		//Enter Interested Party
		lgn.locateElementByXPath("//*[@id=\'react-select-4--value\']", LoadProperties.cTenIntParty);

		Thread.sleep(1000);

		//Select Interested Party
		WebElement intParty = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[1]/div[1]/div[4]/div[2]/div/div[1]/div[2]"));
		action.moveToElement(intParty);
		action.sendKeys(Keys.TAB);
		action.perform();

		Thread.sleep(1000);

		//Select Severty Dropdown
		sevDrop.click();

		//Select Severty
		option2.click();

		JavascriptExecutor jse = (JavascriptExecutor)dr;
		jse.executeScript("scroll(0, 700)");

		Thread.sleep(1000);

		// Click on Save
		save.click();

		Thread.sleep(2000);

		try {
			success = toastMsg.getText();
			logs.printLogsToFile(testCase+ success);
		}catch(Exception e) {                  
			warning = warn.getText();
			logs.printLogsToFile(testCase+ warning);
		}
	}
	@AfterMethod
	public void afterMethod(){
		dr.quit();
	}
}
