package employees;

import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import launch.Login;
import properties.CaptureOutput;
import properties.HandleLogs;
import properties.LoadProperties;

public class CreateEmployee extends LoadProperties {

	WebDriver dr;
	String getToasterMsg;
	String testCase = "Employee Creation ---> ";
	Login lgn = new Login();

	@BeforeMethod
	public void beforeMethod() {
		lgn.login();
		dr = lgn.getLoginDriverInstance();
	}

	@Test
	public void createEmployee() throws InterruptedException, IOException {

		CaptureOutput res = new CaptureOutput();

		HandleLogs logs = new HandleLogs();

		Actions action = new Actions(dr);

		logs.printLogsToFile("********** " + testCase + "Started ***********" + new Date());

		//Click on Employee Management Menu
		employeeMenu.click();

		//Click on Create Employee button
		createBtn.click();

		//Enter DOB
		doB.sendKeys(LoadProperties.empDob);

		//Enter Emp Name
		name.sendKeys(LoadProperties.empName);

		//Enter Father's Name
		fName.sendKeys(LoadProperties.empFaName);

		//Enter Mobile Number
		mobile.sendKeys(LoadProperties.empMobile);

		//Enter Aadhar Number
		aadharNumber.sendKeys(LoadProperties.empAadhar);

		//Enter ESI Number
		esi.sendKeys(LoadProperties.empEsi);

		//Enter PF Number
		pf.sendKeys(LoadProperties.empPf);

		//Enter UAN Number
		uan.sendKeys(LoadProperties.empUAN);

		//Enter Email ID
		email.sendKeys(LoadProperties.empEmail);

		//Enter Password
		pwd.sendKeys(LoadProperties.empPassword);

		JavascriptExecutor jse = (JavascriptExecutor)dr;
		jse.executeScript("scroll(0, 250)");		
		Thread.sleep(1000);

		//Enter House Number
		hNumber.sendKeys(LoadProperties.empHouseNumber);

		//Enter Street / Sector / Post Office
		pACity.sendKeys(LoadProperties.empStreet);

		//Enter Police Station
		pStation.sendKeys(LoadProperties.empPoliceStation);

		//Enter District
		dist.sendKeys(LoadProperties.empDistrict);

		//Select State
		stateDrop.click();
		option9.click();

		Thread.sleep(1000);

		//Enter Landline Number
		landline.sendKeys(LoadProperties.empLandLine);
		Thread.sleep(1000);

		//Enter Country
		cntry.clear();
		cntry.sendKeys(LoadProperties.empCountry);

		//Enter Postal Code
		postal.sendKeys(LoadProperties.empPostalCode);

		//Select Checkbox
		dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[1]/div[8]/div[1]/span/span[1]/input")).click();

		JavascriptExecutor jsex = (JavascriptExecutor)dr;
		jsex.executeScript("scroll(0, 700)");

		//Click on Next
		next.click();

		WebDriverWait wait = new WebDriverWait(dr,5);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("dateOfJoing")));

		//Service Details
		//Select DOJ
		doJ.sendKeys(LoadProperties.empDOJ);

		//Select Role
		lgn.locateElementByXPath("//*[@id=\'react-select-2--value\']/div[1]",LoadProperties.empRole);
		Thread.sleep(1000);
		WebElement selectRole = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[2]/div[3]/div[1]/div[2]"));
		action.moveToElement(selectRole);
		action.sendKeys(Keys.TAB);
		action.perform();

		Thread.sleep(1000);

		//Select Branch
		lgn.locateElementByXPath("//*[@id=\'react-select-3--value\']",LoadProperties.empBranch);

		Thread.sleep(1000);

		WebElement selectBranch = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[3]/div[1]/div[1]/div[2]"));
		action.moveToElement(selectBranch);
		action.sendKeys(Keys.TAB);
		action.perform();

		WebElement desig = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[3]/div[2]/div[1]/div/div"));
		action.moveToElement(desig);
		action.click();
		action.perform();

		//Select Designation Option1
		option1.click();

		Thread.sleep(1000);

		//Enter CTC
		ctc.sendKeys(LoadProperties.empCTC);

		//Click on Save Button
		saveDetails.click();

		Thread.sleep(2000);

		String expected = LoadProperties.url+"/employee-details";				
		boolean actual = dr.getCurrentUrl().equals(expected);

		if(!actual){
			getToasterMsg = toasterMsg.getText();
			res.getScreenshot(dr);
			logs.printLogsToFile(testCase +getToasterMsg);
			logs.printLogsToFile("********** " + testCase + "Finished ***********");
			Assert.assertEquals(actual, expected,  testCase+"Employee creation failed!");
		}
		else{
			getToasterMsg = toasterMsg.getText();
			logs.printLogsToFile(testCase +getToasterMsg);
			logs.printLogsToFile("**********" + testCase + "Finished ***********");
		}
	}
	@AfterMethod
	public void afterMethod(){
		dr.quit();
	}
}
