package employees;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import launch.Login;
import properties.HandleLogs;
import properties.LoadProperties;

public class ChangePassword extends LoadProperties{

	WebDriver dr;
	String emptySrch;
	String noPassMatch;
	String testCase = "Change Password --->";
	
	Login lgn = new Login();
	
	@BeforeMethod
	public void beforeMethod() {
		lgn.login();
		dr = lgn.getLoginDriverInstance();
	}

	@Test
	public void changeEmpPassword() throws InterruptedException {

		HandleLogs log = new HandleLogs();

		log.printLogsToFile("********** " + testCase + "Started ***********"+ new Date());

		//Click on Employee Management menu
		employeeMenu.click();

		//Click on Search Icon
		searchIcon.click();

		//Search for an Employee
		search.sendKeys(LoadProperties.searchEmp);

		//Click on Search Now button
		searchNow.click();
		
		Thread.sleep(1000);

		emptySrch = emptySearch.getText();

		if(emptySrch.contains("No result found for the above search. Please try again")) {
			log.printLogsToFile(testCase +emptySrch);
		}else
		{
			//Click on edit icon
			editIcon.click();

			//Click on Change Password link
			chPassLink.click();

			//Enter New Password
			passNew.sendKeys(LoadProperties.newPass);

			//Enter Confirm Password
			passConfirm.sendKeys(LoadProperties.conPass);

			//Click on Change Button
			change.click();	

			Thread.sleep(1000);
			
			//Check the confirm password and new password
			
			try {
				noPassMatch = passNotMatch.getText();
				log.printLogsToFile(testCase +noPassMatch);
			}catch(Exception e) {
				log.printLogsToFile(testCase+"Password successfully changed!");
				log.printLogsToFile("********** " + testCase + "Finished ***********");
			}
		}
	}
	@AfterMethod
	public void afterMethod(){
		dr.quit();
	}
}