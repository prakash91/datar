
package launch;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import properties.HandleLogs;
import properties.LaunchBrowser;
import properties.LoadProperties;

public class Login extends LoadProperties {

	WebDriver dr;
	String testCase = "Login ---> ";

	@Test
	public WebDriver login(){
		
		LaunchBrowser log = new LaunchBrowser();
		log.launchBrowser();
		dr = log.getWebDriverInstance();
		
		PageFactory.initElements(dr, Login.class);

		HandleLogs logger = new HandleLogs();

		logger.printLogsToFile("**********"+testCase+"Started **********"+ new Date());
		
		//Enter Email
		email.sendKeys(LoadProperties.username);

		//Enter Password
		pwd.sendKeys(LoadProperties.password);

		//Click on Login button
		lgnBtn.click();

		WebDriverWait wait = new WebDriverWait(dr, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[1]/a/span")));

		String expected = dashboard;
		boolean actual = dr.getCurrentUrl().equals(expected);

		if(actual){
			logger.printLogsToFile(testCase +"Login Successful! Welcome to datar admin panel");
			logger.printLogsToFile("********** " + testCase + "Finished ***********");
		}
		else{
			logger.printLogsToFile(testCase +"Login Failed! Invalid Email or Password");
			logger.printLogsToFile("********** " + testCase + "Finished ***********");
			Assert.assertEquals(actual,expected,"Invalid Email or Password");
		}
		return dr;
	}
	public WebDriver getLoginDriverInstance() {
		return dr;
	}
	public void locateElementByName(String selector, String value) {
		Actions action = new Actions(dr);
		WebElement ele = dr.findElement(By.name(selector));
		action.moveToElement(ele);
		action.click();
		action.sendKeys(value);
		action.perform();
	}
	public void locateElementByXPath(String selector, String value) {
		Actions action = new Actions(dr);
		WebElement ele = dr.findElement(By.xpath(selector));
		action.moveToElement(ele);
		action.click();
		action.sendKeys(value);
		action.perform();
	}
}
