package properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.Test;

public class HandleLogs {

	public static final Logger logger = Logger.getLogger(HandleLogs.class);

	@Test
	public void printLogsToFile(String msg) {
		try {
			PropertyConfigurator.configure("log4j.properties");
			logger.info(msg);
		} catch (SecurityException e) {
			logger.info(e);
		}
	}
}
