package properties;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

public class LaunchBrowser extends LoadProperties {
	WebDriver dr;

	String testCase = "Launch Browser ---> ";
	
	public WebDriver launchBrowser() {
		
		HandleLogs logger = new HandleLogs();
		
		if(browser.equalsIgnoreCase("Chrome"))
		{
			logger.printLogsToFile("------- Running Tests In Google Chrome Browser -------");
			System.setProperty("webdriver.chrome.driver", "../datar/chromedriver");
			dr = new ChromeDriver();
			dr.get(url);
			dr.manage().window().maximize();
			dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		else if(browser.equalsIgnoreCase("Firefox")) 
		{
			logger.printLogsToFile("------- Running Tests In Mozilla Firefox Browser -------");
			System.setProperty("webdriver.gecko.driver", "../datar/geckodriver");
			dr = new FirefoxDriver();
			dr.get(url);
			dr.manage().window().maximize();
			dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		else if(browser.equalsIgnoreCase("Safari"))
		{
			logger.printLogsToFile("------- Running Tests In Safari Browser -------");
			dr = new SafariDriver();
			dr.get(url);
			dr.manage().window().maximize();
			dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		else if(browser.equalsIgnoreCase("InternetExplorer"))
		{
			logger.printLogsToFile("------- Running Tests In Internet Explorer Browser -------");
			dr = new InternetExplorerDriver();
			dr.get(url);
			dr.manage().window().maximize();
			dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		else
		{
			logger.printLogsToFile("????? Please provide a browser ?????");
		}
		return dr;
	}
	public WebDriver getWebDriverInstance() {
		return dr;
	}
}
