package properties;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CaptureOutput extends LoadProperties{
	public void getScreenshot(WebDriver dr) throws IOException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ");
		String dateString = format.format(new Date());
		File source = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
		File dest = new File("../datar/Screenshots/"+dateString+".png");
		FileUtils.copyFile(source, dest);
	 }
}