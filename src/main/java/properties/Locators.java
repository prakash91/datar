package properties;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Locators {

	//Login Page Locators

	//Email
	@FindBy(how = How.NAME, using = "email")
	protected static WebElement email;

	//Password
	@FindBy(how = How.NAME, using = "password")
	protected static WebElement pwd;

	//Login Button
	@FindBy(how = How.XPATH, using = "//*[@id='root']/div/div[2]/form/button")
	protected static WebElement lgnBtn;

	//Complaints Locators

	//Complaint Management Menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[8]/a/span")
	protected static WebElement complaintMenu;

	//Task Management Menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[9]/a/span")
	protected static WebElement taskMenu;

	//Tender Management Menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[10]/a/span")
	protected static WebElement tenderMenu;

	//Create Complaint Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div/a/button/span[1]/span")
	protected static WebElement create;

	//Parent Radio Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/div/div[2]/div")
	protected static WebElement parentRadio;

	//Description
	@FindBy(how = How.NAME, using = "taskDescription")
	protected static WebElement description;

	//Due Date
	@FindBy(how = How.NAME, using = "date")
	protected static WebElement date;	

	//Expected Hours
	@FindBy(how = How.NAME, using = "expectedHours")
	protected static WebElement expectedHours;	

	//Severity Drop Down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[1]/div[1]/div[5]/div/div/div[1]/div/div")
	protected static WebElement sevDrop;	

	//Severity
	@FindBy(how = How.XPATH, using = "//*[@id=\'menu-\']/div[2]/ul/li[2]")
	protected static WebElement option2;	

	//Save Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[2]/div/button")
	protected static WebElement save;
	
	//Warning
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div/div[1]/div/div/p")
	protected static WebElement warn;

	//CRM Locators -- Create Client

	//CRM Menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[7]/a/span")
	protected static WebElement crmMenu;

	//Add Client, Create Employee Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div/div[1]/div[1]/div[2]/div/a/button/span[1]/span")
	protected static WebElement createBtn;

	//Client Name, Employee Name
	@FindBy(how = How.NAME, using = "name")
	protected static WebElement name;

	//Contact Name
	@FindBy(how = How.NAME, using = "contactName")
	protected static WebElement contact;	

	//Contact Number
	@FindBy(how = How.NAME, using = "contactNumber")
	protected static WebElement conNumber;	

	//Contact Designation
	@FindBy(how = How.NAME, using = "contactDesignation")
	protected static WebElement contactDesig;	

	//Create Client Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[3]/button")
	protected static WebElement createClientBtn;

	//Toaster Msg on Duplicate Client
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[1]/div/div[1]/div[1]/div[2]")
	protected static WebElement toaster;

	//Add Service Option              
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div/button/span[1]/span")
	protected static WebElement addService;

	//Service Name
	@FindBy(how = How.NAME, using = "defaultServices[0].serviceName")
	protected static WebElement svcName;

	//Wage Rate Drop Down              
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[1]/div/div/div[1]/div[2]/div/div[2]/div[1]/div/div")
	protected static WebElement wageDrop;	

	//Select Rate Type
	@FindBy(how = How.XPATH, using = "//*[@id='menu-']/div[2]/ul/li[3]")
	protected static WebElement rType;		

	//Wage Rate
	@FindBy(how = How.NAME, using = "defaultServices[0].wageRate.rate")
	protected static WebElement wgRate;

	//OT Wage Rate Drop Down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[1]/div/div/div[1]/div[2]/div/div[4]/div[1]/div/div")
	protected static WebElement oTWageDrop;	

	//OT Wage Rate
	@FindBy(how = How.NAME, using = "defaultServices[0].overTimeWageRate.rate")
	protected static WebElement oTWgRate;

	//Billing Rate Drop Down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[1]/div/div/div[1]/div[3]/div/div[2]/div[1]/div/div")
	protected static WebElement billingDrop;	

	//Billing Rate
	@FindBy(how = How.NAME, using = "defaultServices[0].billingRate.rate")
	protected static WebElement billRate;

	//OT Billing Rate Drop Down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[1]/div/div/div[1]/div[3]/div/div[4]/div[1]/div/div")
	protected static WebElement oTBillDrop;	

	//OT Bill Rate
	@FindBy(how = How.NAME, using = "defaultServices[0].overTimeBillingRate.rate")
	protected static WebElement oTBillRate;

	//Service Charge Drop Down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[1]/div/div/div[1]/div[4]/div/div[2]/div[1]/div/div")
	protected static WebElement serviceDrop;			

	//Service Rate
	@FindBy(how = How.NAME, using = "defaultServices[0].serviceCharge")
	protected static WebElement svcRate;

	//SAC Code
	@FindBy(how = How.NAME, using = "defaultServices[0].sacCode")
	protected static WebElement sCode;

	//Service Hours
	@FindBy(how = How.NAME, using = "defaultServices[0].serviceHours")
	protected static WebElement svcHours;

	//Add Service Option 2
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[2]/button")
	protected static WebElement addServiceO2;

	//Service Name2
	@FindBy(how = How.NAME, using = "defaultServices[1].serviceName")
	protected static WebElement svcName2;

	//Wage Rate Drop Down2
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div[2]/div[1]/div/div")
	protected static WebElement wageDrop2;	

	//Wage Rate2
	@FindBy(how = How.NAME, using = "defaultServices[1].wageRate.rate")
	protected static WebElement wgRate2;

	//OT Wage Rate Drop Down2          
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[2]/div/div/div[1]/div[2]/div/div[4]/div[1]/div/div")
	protected static WebElement oTWageDrop2;	

	//OT Wage Rate2
	@FindBy(how = How.NAME, using = "defaultServices[1].overTimeWageRate.rate")
	protected static WebElement oTWgRate2;

	//Billing Rate Drop Down2
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[2]/div/div/div[1]/div[3]/div/div[2]/div[1]/div/div")
	protected static WebElement billingDrop2;	

	//Billing Rate2
	@FindBy(how = How.NAME, using = "defaultServices[1].billingRate.rate")
	protected static WebElement billRate2;

	//OT Billing Rate Drop Down2
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[2]/div/div/div[1]/div[3]/div/div[4]/div[1]/div/div")
	protected static WebElement oTBillDrop2;	

	//OT Bill Rate2
	@FindBy(how = How.NAME, using = "defaultServices[1].overTimeBillingRate.rate")
	protected static WebElement oTBillRate2;

	//Service Charge Drop Down2       
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[2]/div/div/div[1]/div[4]/div/div[2]/div[1]/div/div")
	protected static WebElement serviceDrop2;		

	//Service Rate2
	@FindBy(how = How.NAME, using = "defaultServices[1].serviceCharge")
	protected static WebElement svcRate2;

	//SAC Code2
	@FindBy(how = How.NAME, using = "defaultServices[1].sacCode")
	protected static WebElement sCode2;

	//Service Hours2
	@FindBy(how = How.NAME, using = "defaultServices[1].serviceHours")
	protected static WebElement svcHours2;


	//CRM Locators -- Create Branch

	//Click on Branch Menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[7]/ul/li[2]/a/span")
	protected static WebElement bMenu;

	//Enter Branch Name
	@FindBy(how = How.NAME, using = "clientBranch.branchName")
	protected static WebElement bName;

	//Enter Branch Contact Number
	@FindBy(how = How.NAME, using = "clientBranch.branchContactNumber")
	protected static WebElement bNumber;

	//Enter Branch Email ID
	@FindBy(how = How.NAME, using = "clientBranch.branchEmail")
	protected static WebElement bEmail;

	//Enter Branch Contant Name
	@FindBy(how = How.NAME, using = "clientBranch.branchContactName")
	protected static WebElement bConName;

	//Enter Branch Designation
	@FindBy(how = How.NAME, using = "clientBranch.branchContactDesignation")
	protected static WebElement bDesig;

	//Enter Branch GST Number
	@FindBy(how = How.NAME, using = "clientBranch.gstNumber")
	protected static WebElement bGST;

	//Enter Village/House Number
	@FindBy(how = How.NAME, using = "clientBranch.address.villageOrHouseNo")
	protected static WebElement bHouse;

	//Enter Postal Code
	@FindBy(how = How.NAME, using = "clientBranch.address.postalCode")
	protected static WebElement bPostal;

	//Enter Police Station
	@FindBy(how = How.NAME, using = "clientBranch.address.policeStation")
	protected static WebElement bPolice;

	//Enter District
	@FindBy(how = How.NAME, using = "clientBranch.address.district")
	protected static WebElement bDist;

	//Invoice Locators

	//Click on Billing and Invoices menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[5]/a/span")
	protected static WebElement billInvoiceMenu;

	//Click on Generate Invoice button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div/a/button/span[1]/span")
	protected static WebElement genInvoice;

	//Click on Invoice Type Drop down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[1]/div[1]/div/div[1]/div/div")
	protected static WebElement invoiceTypeDrop;

	//Select Branch as Invoice Type
	@FindBy(how = How.XPATH, using = "//*[@id=\'menu-\']/div[2]/ul/li[1]")
	protected static WebElement option1;

	//Select District as Invoice Type
	@FindBy(how = How.XPATH, using = "//*[@id=\'menu-\']/div[2]/ul/li[3]")
	protected static WebElement disInvoiceType;

	//Click on Expand Icon
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[2]/div/span")
	protected static WebElement expand;

	//Enter Invoice Number
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[6]/div[2]/div[1]/div[2]/input")
	protected static WebElement invoiceNumber;

	//Enter Purchase Order
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[6]/div[2]/div[1]/div[3]/input")
	protected static WebElement purOrder;

	//Submit the Invoice
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[7]/button")
	protected static WebElement invSubmit;

	//Invoice Toaster
	@FindBy(how = How.ID, using = "toast-container")
	protected static WebElement toasterMsg;

	//Task,Tender, Complaints Toaster
	@FindBy(how = How.CLASS_NAME, using = "toast-message")
	protected static WebElement toastMsg;

	//Generate Salary -- Locators

	//Click on HR & Payroll menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[4]/a/span")
	protected static WebElement hrPayrollMenu;

	//Click on Generate Salary Link
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[4]/ul/li[2]/a/span")
	protected static WebElement generateSalary;

	//Click on Select Report type drop down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[2]/form/div[1]/div/div[1]/div/div")
	protected static WebElement rTypeDrop;

	//Enter Total Working Days
	@FindBy(how = How.NAME, using = "totalWorkingDays")
	protected static WebElement totalWorkDays;	

	//Click on Generate Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[2]/form/div[2]/div/div/button")
	protected static WebElement genSalary;


	//Employee Management -- Locators

	//Click on Employee Management menu
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[1]/div/ul/li[2]/a/span")
	protected static WebElement employeeMenu;

	//Enter DOB
	@FindBy(how = How.NAME, using = "dateOfBirth")
	protected static WebElement doB;	

	//Enter Father's Name
	@FindBy(how = How.NAME, using = "spouseOrFathersName")
	protected static WebElement fName;

	//Enter Mobile Number
	@FindBy(how = How.NAME, using = "mobile")
	protected static WebElement mobile;

	//Enter DOB
	@FindBy(how = How.NAME, using = "aadharNumber")
	protected static WebElement aadharNumber;	

	//Enter Father's Name
	@FindBy(how = How.NAME, using = "esiNumber")
	protected static WebElement esi;

	//Enter Mobile Number
	@FindBy(how = How.NAME, using = "pfNumber")
	protected static WebElement pf;

	//Enter Mobile Number
	@FindBy(how = How.NAME, using = "uan")
	protected static WebElement uan;

	//Enter House Number
	@FindBy(how = How.NAME, using = "permanentAddress.villageOrHouseNo")
	protected static WebElement hNumber;	

	//Enter Street / Sector / Post Office
	@FindBy(how = How.NAME, using = "permanentAddress.city")
	protected static WebElement pACity;

	//Enter Police Station		
	@FindBy(how = How.NAME, using = "permanentAddress.policeStation")
	protected static WebElement pStation;

	//Enter District
	@FindBy(how = How.NAME, using = "permanentAddress.district")
	protected static WebElement dist;

	//Click on State Drop Down
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[1]/div[7]/div[3]/div[2]/div[1]/div")
	protected static WebElement stateDrop;

	//Select State Value Option 9
	@FindBy(how = How.XPATH, using = "//*[@id=\'menu-\']/div[2]/ul/li[9]")
	protected static WebElement option9;

	//Enter Landline Number
	@FindBy(how = How.NAME, using = "permanentAddress.phoneNo")
	protected static WebElement landline;

	//Enter Country
	@FindBy(how = How.NAME, using = "permanentAddress.country")
	protected static WebElement cntry;

	//Enter District
	@FindBy(how = How.NAME, using = "permanentAddress.postalCode")
	protected static WebElement postal;

	//Click on Next Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[2]/div/button/span[1]")
	protected static WebElement next;

	//Service Details
	//Enter DOJ
	@FindBy(how = How.NAME, using = "dateOfJoing")
	protected static WebElement doJ;

	//Enter CTC
	@FindBy(how = How.NAME, using = "ctc")
	protected static WebElement ctc;

	//Click on Save Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div[3]/div/button/span[1]")
	protected static WebElement saveDetails;

	//Change Password
	//Click on Search Icon
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div/div[1]/div[2]/div[2]/a/img")
	protected static WebElement searchIcon;

	//Search for an Employee
	@FindBy(how = How.NAME, using = "search")
	protected static WebElement search;

	//Click on Search Now button
	@FindBy(how = How.XPATH, using = "/html/body/div[2]/div[2]/div[2]/div[3]/form/div[2]/div/button/span[1]")
	protected static WebElement searchNow;

	//Empty Search Result
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div/div[3]/div")
	protected static WebElement emptySearch;

	//Click on Edit Icon
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div/div[3]/div[1]/div[7]/div[1]/span/img")
	protected static WebElement editIcon;

	//Click on Change Password Link
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/form/div/div[1]/div[5]/span")
	protected static WebElement chPassLink;

	//Enter New Password
	@FindBy(how = How.NAME, using = "newPassword")
	protected static WebElement passNew;

	//Enter Confirm Password
	@FindBy(how = How.NAME, using = "confirmPassword")
	protected static WebElement passConfirm;

	//Click on Change Button
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div[2]/button/p")
	protected static WebElement change;
	
	//No Password Match Error
	@FindBy(how = How.XPATH, using = "//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div[3]/div/form/div[2]/p")
	protected static WebElement passNotMatch;


	Locators(){

	}
}
