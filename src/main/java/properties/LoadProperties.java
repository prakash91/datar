package properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeSuite;

public class LoadProperties extends Locators  {

	protected static String browser;
	protected static String url;
	protected static String username;
	protected static String password;
	protected static String dashboard;

	//Employee Details
	protected static String empName;
	protected static String empFaName;
	protected static String empDob;
	protected static String empMobile;
	protected static String empAadhar;
	protected static String empEsi;
	protected static String empPf;
	protected static String empUAN;
	protected static String empEmail;
	protected static String empPassword;
	protected static String empHouseNumber;
	protected static String empStreet;
	protected static String empPoliceStation;
	protected static String empDistrict;
	protected static String empLandLine;
	protected static String empCountry;
	protected static String empPostalCode;

	//Service Details
	protected static String empDOJ;
	protected static String empRole;
	protected static String empBranch;
	protected static String empCTC;

	//Update Password
	protected static String searchEmp;
	protected static String newPass;
	protected static String conPass;


	//Client Details
	protected static String clientName;
	protected static String contactName;
	protected static String contactNumber;
	protected static String contactDesignation;
	protected static String contactEmail;

	//Service Details
	protected static String serviceName;
	protected static String wageRate;
	protected static String oTWageRate;
	protected static String billingRate;
	protected static String oTBillingRate;
	protected static String serviceCharge;
	protected static String sacCode;
	protected static String serviceHours;
	protected static String rateType;
	protected static String serviceChargeType;

	//Service Details2
	protected static String serviceName2;
	protected static String wageRate2;
	protected static String oTWageRate2;
	protected static String billingRate2;
	protected static String oTBillingRate2;
	protected static String serviceCharge2;
	protected static String sacCode2;
	protected static String serviceHours2;

	//Branch Details
	protected static String branchName;
	protected static String bContactNumber;
	protected static String bEmailId;
	protected static String bContactName;
	protected static String bContactDesignation;
	protected static String gstNumber;
	protected static String houseNumber;
	protected static String postalCode;
	protected static String policeStation;
	protected static String district;
	protected static String state;
	protected static String city;
	protected static String country;
	protected static String latitude;
	protected static String longitude;

	//BranchWiseBilling & Invoicing
	protected static String invBranch;
	protected static String invFrmDate;
	protected static String invToDate;
	protected static String payeeBName;
	protected static String payeeBank;
	protected static String invoiceNo;
	protected static String purchaseOdr;

	//ClientWiseBilling & Invoicing
	protected static String cWInvClient;
	protected static String cWInvBranch;
	protected static String cWInvFrmDate;
	protected static String cWInvToDate;
	protected static String cWPayeeBName;
	protected static String cWPayeeBank;
	protected static String cWInvoiceNo;
	protected static String cWPurchaseOdr;

	//DistrictWiseBilling & Invoicing
	protected static String dWInvClient;
	protected static String dWInvDistrict;
	protected static String dWInvFrmDate;
	protected static String dWInvToDate;
	protected static String dWPayeeBName;
	protected static String dWPayeeBank;
	protected static String dWInvoiceNo;
	protected static String dWPurchaseOdr;

	//Generate Salary for Client
	protected static String gSClient;
	protected static String gSMonth;
	protected static String gSYear;
	protected static String gSTotalWorkDays;

	//Generate Salary for Employee
	protected static String gSEmp;
	protected static String gSEMonth;
	protected static String gSEYear;
	protected static String gSETotalWorkDays;

	//Complaint Management - Parent
	protected static String pComplaint;
	protected static String dueDate;
	protected static String assignee;
	protected static String expHours;
	protected static String intParty;

	//Complaint Management - Child
	protected static String cComplaint;
	protected static String cDueDate;
	protected static String cAssignee;
	protected static String cExpHours;
	protected static String cIntParty;

	//Task Management - Parent
	protected static String pTask;
	protected static String pDueDate;
	protected static String pAssignee;
	protected static String pExpHours;
	protected static String pIntParty;

	//Task Management - Child
	protected static String cTask;
	protected static String cTDueDate;
	protected static String cTAssignee;
	protected static String cTExpHours;
	protected static String cTIntParty;

	//Tender Management - Parent
	protected static String pTender;
	protected static String pTenDueDate;
	protected static String pTenAssignee;
	protected static String pTenExpHours;
	protected static String pTenIntParty;

	//Tender Management - Child
	protected static String cTender;
	protected static String cTenDueDate;
	protected static String cTenAssignee;
	protected static String cTenExpHours;
	protected static String cTenIntParty;

	@BeforeSuite
	public static void loadProperties() throws IOException {

		Properties prop = new Properties();
		FileInputStream fs = new FileInputStream("../datar/config.properties");
		prop.load(fs);

		browser = prop.getProperty("browser");
		url = prop.getProperty("url");
		username = prop.getProperty("email");
		password = prop.getProperty("password");
		dashboard = prop.getProperty("dashboard");

		//Employee Details
		empName = prop.getProperty("empName");
		empFaName = prop.getProperty("empFaName");
		empDob = prop.getProperty("empDob");
		empMobile = prop.getProperty("empMobile");
		empAadhar = prop.getProperty("empAadhar");
		empEsi =prop.getProperty("empEsi");
		empPf = prop.getProperty("empPf");
		empUAN = prop.getProperty("empUAN");
		empEmail = prop.getProperty("empEmail");
		empPassword = prop.getProperty("empPassword");
		empHouseNumber = prop.getProperty("empHouseNumber");
		empStreet = prop.getProperty("empStreet");
		empPoliceStation = prop.getProperty("empPoliceStation");
		empDistrict = prop.getProperty("empDistrict");
		empLandLine = prop.getProperty("empLandLine");
		empCountry = prop.getProperty("empCountry");
		empPostalCode = prop.getProperty("empPostalCode");

		//Employee Service Details
		empDOJ = prop.getProperty("empDOJ");
		empRole = prop.getProperty("empRole");
		empBranch =prop.getProperty("empBranch");
		empCTC = prop.getProperty("empCTC");

		//Update Password
		searchEmp = prop.getProperty("searchEmp");
		newPass = prop.getProperty("newPass");
		conPass = prop.getProperty("conPass");

		//Client Details
		clientName = prop.getProperty("clientName");
		contactName = prop.getProperty("contactName");
		contactNumber = prop.getProperty("contactNumber");
		contactDesignation = prop.getProperty("contactDesignation");
		contactEmail = prop.getProperty("contactEmail");

		//Service Details
		serviceName = prop.getProperty("serviceName");
		wageRate = prop.getProperty("wageRate");
		oTWageRate = prop.getProperty("oTWageRate");
		billingRate = prop.getProperty("billingRate");
		oTBillingRate = prop.getProperty("oTBillingRate");
		serviceCharge = prop.getProperty("serviceCharge");
		sacCode = prop.getProperty("sacCode");
		serviceHours = prop.getProperty("serviceHours");
		rateType = prop.getProperty("rateType");
		serviceChargeType = prop.getProperty("serviceChargeType");

		//Service Details
		serviceName2 = prop.getProperty("serviceName2");
		wageRate2 = prop.getProperty("wageRate2");
		oTWageRate2 = prop.getProperty("oTWageRate2");
		billingRate2 = prop.getProperty("billingRate2");
		oTBillingRate2 = prop.getProperty("oTBillingRate2");
		serviceCharge2 = prop.getProperty("serviceCharge2");
		sacCode2 = prop.getProperty("sacCode2");
		serviceHours2 = prop.getProperty("serviceHours2");

		//Branch Details
		branchName = prop.getProperty("branchName");
		bContactNumber = prop.getProperty("bContactNumber");
		bEmailId = prop.getProperty("bEmailId");
		bContactName = prop.getProperty("bContactName");
		bContactDesignation = prop.getProperty("bContactDesignation");
		gstNumber = prop.getProperty("gstNumber");
		houseNumber = prop.getProperty("houseNumber");
		postalCode = prop.getProperty("postalCode");
		policeStation =  prop.getProperty("policeStation");
		district = prop.getProperty("district");
		state = prop.getProperty("state");
		city = prop.getProperty("city");
		country = prop.getProperty("country");
		latitude = prop.getProperty("latitude");
		longitude = prop.getProperty("longitude");

		//BranchWiseBilling & Invoicing
		invBranch = prop.getProperty("invBranch");
		invFrmDate = prop.getProperty("invFrmDate");
		invToDate = prop.getProperty("invToDate");
		payeeBName = prop.getProperty("payeeBName");
		payeeBank = prop.getProperty("payeeBank");
		invoiceNo = prop.getProperty("invoiceNo");
		purchaseOdr = prop.getProperty("purchaseOdr");

		//ClientWiseBilling & Invoicing
		cWInvClient = prop.getProperty("cWInvClient");
		cWInvBranch = prop.getProperty("cWInvBranch");
		cWInvFrmDate = prop.getProperty("cWInvFrmDate");
		cWInvToDate = prop.getProperty("cWInvToDate");
		cWPayeeBName = prop.getProperty("cWPayeeBName");
		cWPayeeBank = prop.getProperty("cWPayeeBank");
		cWInvoiceNo = prop.getProperty("cWInvoiceNo");
		cWPurchaseOdr = prop.getProperty("cWPurchaseOdr");

		//DistrictWiseBilling & Invoicing
		dWInvClient = prop.getProperty("dWInvClient");
		dWInvDistrict = prop.getProperty("dWInvDistrict");
		dWInvFrmDate = prop.getProperty("dWInvFrmDate");
		dWInvToDate = prop.getProperty("dWInvToDate");
		dWPayeeBName = prop.getProperty("dWPayeeBName");
		dWPayeeBank = prop.getProperty("dWPayeeBank");
		dWInvoiceNo = prop.getProperty("dWInvoiceNo");
		dWPurchaseOdr = prop.getProperty("dWPurchaseOdr");

		//Generate Salary for Client
		gSClient = prop.getProperty("gSClient");
		gSMonth = prop.getProperty("gSMonth");
		gSYear = prop.getProperty("gSYear");
		gSTotalWorkDays = prop.getProperty("gSTotalWorkDays");

		//Generate Salary for Employee
		gSEmp = prop.getProperty("gSEmp");
		gSEMonth = prop.getProperty("gSEMonth");
		gSEYear = prop.getProperty("gSEYear");
		gSETotalWorkDays = prop.getProperty("gSETotalWorkDays");

		//Complaint Management - Parent
		pComplaint = prop.getProperty("pComplaint");
		dueDate = prop.getProperty("dueDate");
		assignee = prop.getProperty("assignee");
		expHours = prop.getProperty("expHours");
		intParty = prop.getProperty("intParty");

		//Complaint Management - Child
		cComplaint = prop.getProperty("cComplaint");
		cDueDate = prop.getProperty("cDueDate");
		cAssignee = prop.getProperty("cAssignee");
		cExpHours = prop.getProperty("cExpHours");
		cIntParty = prop.getProperty("cIntParty");

		//Task Management - Parent
		pTask = prop.getProperty("pTask");
		pDueDate = prop.getProperty("pDueDate");
		pAssignee = prop.getProperty("pAssignee");
		pExpHours = prop.getProperty("pExpHours");
		pIntParty = prop.getProperty("pIntParty");

		//Task Management - Child
		cTask = prop.getProperty("cTask");
		cTDueDate = prop.getProperty("cTDueDate");
		cTAssignee = prop.getProperty("cTAssignee");
		cTExpHours = prop.getProperty("cTExpHours");
		cTIntParty = prop.getProperty("cTIntParty");

		//Tender Management - Parent
		pTender = prop.getProperty("pTender");
		pTenDueDate = prop.getProperty("pTenDueDate");
		pTenAssignee = prop.getProperty("pTenAssignee");
		pTenExpHours = prop.getProperty("pTenExpHours");
		pTenIntParty = prop.getProperty("pTenIntParty");

		//Tender Management - Child
		cTender = prop.getProperty("cTender");
		cTenDueDate = prop.getProperty("cTenDueDate");
		cTenAssignee = prop.getProperty("cTenAssignee");
		cTenExpHours = prop.getProperty("cTenExpHours");
		cTenIntParty = prop.getProperty("cTenIntParty");
	}
}

