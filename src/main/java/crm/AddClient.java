package crm;

import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import launch.Login;
import properties.CaptureOutput;
import properties.HandleLogs;
import properties.LoadProperties;

public class AddClient extends LoadProperties {

	WebDriver dr;
	String testCase = "Add Client ---> ";
	Login lgn = new Login();
	String clientError;

	SoftAssert softAssert = new SoftAssert();

	@BeforeMethod
	public void beforeMethod() {
		lgn.login();
		dr = lgn.getLoginDriverInstance();
	}

	@Test
	public void createClient() throws InterruptedException, IOException {

		CaptureOutput res = new CaptureOutput();

		HandleLogs log = new HandleLogs();

		PageFactory.initElements(dr, AddClient.class);

		log.printLogsToFile("**********"+testCase+"Started **********"+ new Date());

		// Click on CRM
		crmMenu.click();

		//Click on Add Client Button
		createBtn.click();

		//Enter Client Name
		name.sendKeys(LoadProperties.clientName);

		//Enter Contact Name
		contact.sendKeys(LoadProperties.contactName);

		//Enter Contact Number
		conNumber.sendKeys(LoadProperties.contactNumber);

		//Enter Contact Designation
		contactDesig.sendKeys(LoadProperties.contactDesignation);

		//Enter Contact Email
		email.sendKeys(LoadProperties.contactEmail);

		//Click on create button
		WebElement create = createClientBtn;

		if(!create.isEnabled()) {
			clientError = toaster.getText();
			log.printLogsToFile(testCase + clientError);
		}else {
			//********* Add Services **********

			//Expand Add Services option
			addService.click();

			//Enter Service Name
			svcName.sendKeys(LoadProperties.serviceName);

			//Click on Wage Rate Drop Down
			wageDrop.click();
			Thread.sleep(1000);

			//Select Wage Rate Type
			rType.click();
			Thread.sleep(1000);

			//Enter Wage Rate
			wgRate.sendKeys(LoadProperties.wageRate);

			//Click on OT Wage Rate Drop Down
			oTWageDrop.click();
			Thread.sleep(1000);

			//Select on OT Wage Rate Type
			rType.click();
			Thread.sleep(1000);

			//Enter OT Wage Rate
			oTWgRate.sendKeys(LoadProperties.oTWageRate);

			//Click on Billing Rate Drop Down
			billingDrop.click();
			Thread.sleep(1000);

			//Select Billing Rate Type
			rType.click();
			Thread.sleep(1000);

			//Enter Billing Rate
			billRate.sendKeys(LoadProperties.billingRate);

			//Click on OT Billing Rate Type Drop Down
			oTBillDrop.click();
			Thread.sleep(1000);

			//Select OT Billing Rate Type
			rType.click();
			Thread.sleep(1000);

			//Enter OT Billing Rate
			oTBillRate.sendKeys(LoadProperties.oTBillingRate);

			//Click on Service Charge Type
			serviceDrop.click();
			Thread.sleep(1000);

			//Select Service Charge
			option1.click();
			Thread.sleep(1000);

			//Enter Service Charge
			svcRate.sendKeys(LoadProperties.serviceCharge);

			//Enter SAC code
			sCode.sendKeys(LoadProperties.sacCode);

			//Enter Service Hours
			svcHours.sendKeys(LoadProperties.serviceHours);

			Thread.sleep(1000);

			//Add Another Service

			//Expand Add Services option
			addServiceO2.click();

			//Enter Service Name
			dr.findElement(By.name("defaultServices[1].serviceName")).sendKeys(LoadProperties.serviceName2);

			Thread.sleep(1000);

			JavascriptExecutor jse = (JavascriptExecutor)dr;
			jse.executeScript("scroll(0, 500)");

			//Click on Wage Rate Drop Down
			wageDrop2.click();
			Thread.sleep(1000);

			//Select Wage Rate Type2
			rType.click();

			Thread.sleep(1000);

			//Enter Wage Rate
			wgRate2.sendKeys(LoadProperties.wageRate2);

			//Click on OT Wage Rate Drop Down2
			oTWageDrop2.click();
			Thread.sleep(1000);

			//Select OT Wage Rate Type 2
			rType.click();

			Thread.sleep(1000);

			//Enter OT Wage Rate
			oTWgRate2.sendKeys(LoadProperties.oTWageRate2);

			//Click on Billing Rate Type Drop Down 2
			billingDrop2.click();
			Thread.sleep(1000);

			//Select Billing Rate Type2
			rType.click();
			Thread.sleep(1000);

			//Enter Billing Rate
			billRate2.sendKeys(LoadProperties.billingRate2);

			//Click on OT Billing Rate Type Drop Down2
			dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div/div[2]/div[2]/div/div/div[1]/div[3]/div/div[4]/div[1]/div/div")).click();
			Thread.sleep(1000);

			//Select OT Billing Rate Type 2
			rType.click();
			Thread.sleep(1000);

			//Enter OT Billing Rate
			oTBillRate2.sendKeys(LoadProperties.oTBillingRate2);

			//Click on Service Charge Drop Down2
			serviceDrop2.click();
			Thread.sleep(1000);

			//Select Service Charge Type2
			option1.click();
			Thread.sleep(1000);

			//Enter Service Charge
			svcRate2.sendKeys(LoadProperties.serviceCharge2);

			//Enter SAC code
			sCode2.sendKeys(LoadProperties.sacCode2);

			//Enter Service Hours
			svcHours2.sendKeys(LoadProperties.serviceHours2);

			Thread.sleep(1000);

			//Click on Create
			create.click();

			Thread.sleep(2000);

			String expected = url+"/crm/client-list";
			String actual = dr.getCurrentUrl();

			if(actual.equalsIgnoreCase(expected)){
				log.printLogsToFile(testCase +"Client successfully created!");
				log.printLogsToFile("**********" + testCase + "Finished ***********");
			}
			else{
				clientError = toaster.getText();
				log.printLogsToFile(testCase + clientError);
				res.getScreenshot(dr);
				log.printLogsToFile(testCase+"Failed to create client!");
				log.printLogsToFile("********** " + testCase + "Finished ***********");
				softAssert.assertEquals(actual, expected,"Client Creation Failed----->");
				softAssert.assertAll();
			}
		}
	}

	@AfterMethod
	public void afterMethod(){
		dr.quit();
	}
}
