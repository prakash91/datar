package crm;

import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import launch.Login;
import properties.CaptureOutput;
import properties.HandleLogs;
import properties.LoadProperties;

public class AddBranch extends LoadProperties {

	WebDriver dr;
	String errmsg;
	String testCase = "Add Branch ---> ";
	Login lgn = new Login();
	SoftAssert softAssert = new SoftAssert();

	@BeforeMethod
	public void beforeMethod() {
		lgn.login();
		dr = lgn.getLoginDriverInstance();
	}
	
	@Test
	public void createBranch() throws InterruptedException, IOException {

		CaptureOutput res = new CaptureOutput();
		Actions action = new Actions(dr);

		HandleLogs log = new HandleLogs();

		log.printLogsToFile("**********"+testCase+"Started **********" + new Date());
		// Click on CRM
		crmMenu.click();

		//Click on branch option
		bMenu.click();

		//Click on AddNewBranch button
		createBtn.click();

		//Enter Client Name
		lgn.locateElementByXPath("//*[@id=\'react-select-2--value\']/div[1]", LoadProperties.clientName);

		Thread.sleep(1000);

		WebElement selectclient	= dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[1]/div/div/div[2]/div[1]/div[1]/div[2]"));
		action.moveToElement(selectclient);
		action.sendKeys(Keys.TAB);
		action.perform();

		Thread.sleep(1000);

		//Enter Branch Name
		bName.sendKeys(LoadProperties.branchName);

		//Enter Branch Contact Number
		bNumber.sendKeys(LoadProperties.bContactNumber);

		//Enter Branch Email ID
		bEmail.sendKeys(LoadProperties.bEmailId);

		//Enter Branch Contact Name
		bConName.sendKeys(LoadProperties.bContactName);

		//Enter Contact Designation
		bDesig.sendKeys(LoadProperties.bContactDesignation);

		//Enter GST Number
		bGST.sendKeys(LoadProperties.gstNumber);

		//Enter Village/House Number
		bHouse.sendKeys(LoadProperties.houseNumber);

		//Enter Postal Code
		bPostal.sendKeys(LoadProperties.postalCode);

		//Enter Police Station
		bPolice.sendKeys(LoadProperties.policeStation);

		//Enter District
		bDist.sendKeys(LoadProperties.district);

		//Select State
		lgn.locateElementByXPath("//*[@id=\'react-select-4--value\']/div[1]", LoadProperties.state);

		WebElement stateValue = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[2]/div[5]/div[2]/div[1]/div[2]"));
		action.moveToElement(stateValue);
		action.sendKeys(Keys.TAB);
		action.perform();

		//Enter City 
		dr.findElement(By.name("clientBranch.address.city")).sendKeys(LoadProperties.city);

		//Enter Country
		dr.findElement(By.name("clientBranch.address.country")).sendKeys(LoadProperties.country);

		//Enter Latitude
		dr.findElement(By.name("clientBranch.latitude")).sendKeys(LoadProperties.latitude);

		//Enter Longitude
		dr.findElement(By.name("clientBranch.longitude")).sendKeys(LoadProperties.longitude);

		//Click on Save Button
		dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div[3]/button")).click();

		Thread.sleep(2000);

		String expected = LoadProperties.url+"/crm/branch-list";
		boolean actual = dr.getCurrentUrl().equals(expected);

		if(actual){
			WebDriverWait wait = new WebDriverWait(dr, 2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/div/div[1]/div[1]/div[2]/div/a/button/span[1]/span")));
			log.printLogsToFile(testCase +"Branch successfully added!");
			log.printLogsToFile("**********" + testCase + "Finished ***********");
		}
		else{
			errmsg = toasterMsg.getText();
			res.getScreenshot(dr);
			log.printLogsToFile(testCase +errmsg);
			log.printLogsToFile("********** " + testCase + "Finished ***********");
			softAssert.assertEquals(actual, expected, "Add Branch----->Branch creation failed!");
			softAssert.assertAll();
		}
	}
	@AfterMethod
	public void afterMethod(){
		dr.quit();
	}
}