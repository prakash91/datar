package invoice;

import java.io.IOException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import launch.Login;
import properties.CaptureOutput;
import properties.HandleLogs;
import properties.LoadProperties;

public class GenerateInvoice extends LoadProperties {

	WebDriver dr;
	String getToasterMsg;

	Login lgn = new Login();
	CaptureOutput co= new CaptureOutput();
	HandleLogs log = new HandleLogs();

	@BeforeMethod
	public void beforeMethod() {
		lgn.login();
		dr = lgn.getLoginDriverInstance();
	}
	
	@Test(priority=0)
	public void generateBranchWiseInvoice() throws InterruptedException, IOException {
		
		String testCase = "Branch Wise Invoice --->"; 

		Actions action = new Actions(dr);

		log.printLogsToFile("********** " + testCase + "Started ***********" +new Date());

		//Click on Billing and Invoices menu
		billInvoiceMenu.click();

		//Click on Generate Invoice button
		genInvoice.click();

		//Click on Invoice Type
		invoiceTypeDrop.click();

		Thread.sleep(1000);

		//Select Branch as Invoice Type
		option1.click();

		Thread.sleep(1000);		

		//Enter Branch
		lgn.locateElementByXPath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[1]/div/div[2]/div[1]/div[1]/span/span", LoadProperties.invBranch);

		Thread.sleep(1000);

		//Select Branch
		WebElement selectBranch = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[2]/div[1]/div[2]"));

		action.moveToElement(selectBranch);
		action.sendKeys(Keys.TAB);
		action.perform();

		Thread.sleep(1000);

		//Select From Date
		WebElement frmDate = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[1]/div[2]/div[1]/div[1]/div/input"));

		action.moveToElement(frmDate);
		action.click();
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(LoadProperties.invFrmDate);
		action.perform();

		//Select To Date
		WebElement toDate = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[1]/div[2]/div[2]/div[1]/div/input"));

		action.moveToElement(toDate);
		action.click();
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(LoadProperties.invToDate);
		action.perform();

		WebElement fID = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/button"));
		if(!fID.isEnabled()){
			log.printLogsToFile(testCase+"Please fill Mandatory fields!");
			co.getScreenshot(dr);
		}
		else {
			fID.click();

			//Click on Expand Icon
			expand.click();

			//Scroll Down the Page
			JavascriptExecutor jsex = (JavascriptExecutor)dr;
			jsex.executeScript("scroll(0, 700)");

			//Enter Payee Branch Name
			lgn.locateElementByXPath("//*[@id='react-select-3--value']",LoadProperties.payeeBName);

			Thread.sleep(1000);

			//Select Payee Branch
			WebElement payeeBranch = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[4]/div[1]/div[2]"));

			action.moveToElement(payeeBranch);
			action.sendKeys(Keys.ENTER);
			action.perform();

			//Enter Bank
			lgn.locateElementByXPath("//*[@id=\'react-select-4--value\']", LoadProperties.payeeBank);

			Thread.sleep(1000);

			//Select Bank
			WebElement bank = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[5]/div/div[2]/div/div[1]/div[2]"));

			action.moveToElement(bank);
			action.sendKeys(Keys.ENTER);
			action.perform();

			//Scroll Down the Page
			JavascriptExecutor jsex1 = (JavascriptExecutor)dr;
			jsex1.executeScript("scroll(0, 1500)");

			//Enter Invoice Number
			invoiceNumber.sendKeys(LoadProperties.invoiceNo);

			//Enter Purchase Order 
			purOrder.sendKeys(LoadProperties.purchaseOdr);

			//Submit the invoice
			invSubmit.click();

			Thread.sleep(2000);	

			String expected = LoadProperties.url + "/invoice/list";
			boolean actual = dr.getCurrentUrl().equals(expected);

			if(actual){
				getToasterMsg = toasterMsg.getText();
				log.printLogsToFile(testCase +getToasterMsg);
				log.printLogsToFile("********** " + testCase + "Finished ***********");
			}
			else{
				getToasterMsg = toasterMsg.getText();
				co.getScreenshot(dr);
				log.printLogsToFile(testCase +getToasterMsg);
				log.printLogsToFile("********** " + testCase + "Finished ***********");
				Assert.assertEquals(actual,expected,"Branch Wise Invoice----->Invoice generation failed!");
			}
		}

	}

	@Test(priority=1)
	public void generateClientWiseInvoice() throws InterruptedException, IOException {
	
		String testCase = "Client Wise Invoice --->";

		Actions action = new Actions(dr);

		log.printLogsToFile("********** " + testCase + "Started ***********" +new Date());

		//Click on Billing and Invoices menu
		billInvoiceMenu.click();

		//Click on Generate Invoice button
		genInvoice.click();

		//Click on Invoice Type Drop Down
		invoiceTypeDrop.click();

		Thread.sleep(1000);

		//Select Client as Invoice Type
		option2.click();

		Thread.sleep(1000);

		//Enter Client Name
		lgn.locateElementByXPath("//*[@id=\'react-select-3--value\']", LoadProperties.cWInvClient);

		Thread.sleep(1000);

		//Select Client
		WebElement clnt = dr.findElement(By.xpath("//*[@id='root']/div/div[2]/div[2]/div[2]/form/div/div/div[1]/div[1]/div[2]/div[1]/div[2]"));

		action.moveToElement(clnt);
		action.sendKeys(Keys.TAB);
		action.perform();
		Thread.sleep(1000);

		//Enter Branch
		lgn.locateElementByXPath("//*[@id=\'react-select-4--value\']", LoadProperties.cWInvBranch);

		Thread.sleep(1000);

		//Select Branch
		WebElement brnch = dr.findElement(By.xpath("//*[@id=\'root']/div/div[2]/div[2]/div[2]/form/div/div/div[2]/div[1]/div[2]"));

		action.moveToElement(brnch);
		action.sendKeys(Keys.TAB);
		action.perform();
		Thread.sleep(1000);

		//Select From Date
		WebElement cWfrmDate = dr.findElement(By.name("fromDate"));

		action.moveToElement(cWfrmDate);
		action.click();
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(LoadProperties.cWInvFrmDate);
		action.perform();

		//Select To Date
		WebElement cWtoDate = dr.findElement(By.name("toDate"));

		action.moveToElement(cWtoDate);
		action.click();
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(LoadProperties.cWInvToDate);
		action.perform();

		Thread.sleep(1000);

		WebElement fID = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/button"));
		if(!fID.isEnabled()){
			log.printLogsToFile(testCase+"Please fill Mandatory fields!");
			co.getScreenshot(dr);
		}
		else {
			fID.click();
			Thread.sleep(1000);

			//Click on Expand Icon
			expand.click();

			JavascriptExecutor jsex = (JavascriptExecutor)dr;
			jsex.executeScript("scroll(0, 1500)");

			//Enter Payee Branch Name
			lgn.locateElementByXPath("//*[@id=\'react-select-5--value\']",LoadProperties.cWPayeeBName);

			Thread.sleep(1000);

			//Select Payee Branch
			WebElement payeeBranch = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[4]/div[1]/div[2]"));

			action.moveToElement(payeeBranch);
			action.sendKeys(Keys.ENTER);
			action.perform();

			JavascriptExecutor jsex1 = (JavascriptExecutor)dr;
			jsex1.executeScript("scroll(0, 2000)");

			//Enter Bank
			lgn.locateElementByXPath("//*[@id=\'react-select-6--value\']", LoadProperties.cWPayeeBank);

			Thread.sleep(1000);

			//Select Bank
			WebElement bank = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[5]/div/div[2]/div/div[1]/div[2]"));

			action.moveToElement(bank);
			action.sendKeys(Keys.ENTER);
			action.perform();
			Thread.sleep(1000);

			//Enter Invoice Number
			invoiceNumber.sendKeys(LoadProperties.cWInvoiceNo);

			//Enter Purchase Order 
			purOrder.sendKeys(LoadProperties.cWPurchaseOdr);

			//Submit the invoice
			invSubmit.click();

			Thread.sleep(2000);	

			String expected = LoadProperties.url+"/invoice/list";
			boolean actual = dr.getCurrentUrl().equals(expected);

			if(actual){
				getToasterMsg = toasterMsg.getText();
				log.printLogsToFile(testCase +getToasterMsg);
				log.printLogsToFile("********** " + testCase + "Finished ***********");
			}
			else{
				getToasterMsg = toasterMsg.getText();
				co.getScreenshot(dr);
				log.printLogsToFile(testCase +getToasterMsg);
				log.printLogsToFile("********** " + testCase + "Finished ***********");
				Assert.assertEquals(actual,expected,"Generate Client Wise Invoice----->Invoice generation failed!");
			}
		}
	}

	@Test(priority=2)
	public void generateDistrictWiseInvoice() throws InterruptedException, IOException {

		String testCase = "District Wise Invoice ---> ";

		Actions action = new Actions(dr);

		log.printLogsToFile("********** " + testCase + "Started ***********" +new Date());

		//Click on Billing and Invoices menu
		billInvoiceMenu.click();

		//Click on Generate Invoice button
		genInvoice.click();

		//Click on Invoice Type Drop Down
		invoiceTypeDrop.click();

		Thread.sleep(1000);

		//Select District as Invoice Type
		disInvoiceType.click();

		Thread.sleep(1000);

		//Enter Client Name
		lgn.locateElementByXPath("//*[@id='react-select-3--value']", LoadProperties.dWInvClient);

		Thread.sleep(1000);

		//Select Client
		WebElement clnt = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/div/div[2]/div[1]/div[2]"));

		action.moveToElement(clnt);
		action.sendKeys(Keys.TAB);
		action.perform();

		Thread.sleep(1000);

		//Enter District
		dr.findElement(By.name("district")).sendKeys(LoadProperties.dWInvDistrict);

		//Select From Date
		WebElement dWfrmDate = dr.findElement(By.name("fromDate"));

		action.moveToElement(dWfrmDate);
		action.click();
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(LoadProperties.dWInvFrmDate);
		action.perform();

		//Select To Date
		WebElement dWtoDate = dr.findElement(By.name("toDate"));

		action.moveToElement(dWtoDate);
		action.click();
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(Keys.ARROW_LEFT);
		action.sendKeys(LoadProperties.dWInvToDate);
		action.perform();

		Thread.sleep(1000);

		WebElement fID = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div/button"));
		if(!fID.isEnabled()){
			log.printLogsToFile(testCase +"Please fill Mandatory fields!");
			co.getScreenshot(dr);
		}
		else {
			fID.click();
			Thread.sleep(1000);

			//Click on Expand Icon
			expand.click();

			JavascriptExecutor jsex = (JavascriptExecutor)dr;
			jsex.executeScript("scroll(0, 1500)");

			//Enter Payee Branch Name
			lgn.locateElementByXPath("//*[@id=\'react-select-4--value\']",LoadProperties.dWPayeeBName);

			Thread.sleep(1000);

			//Select Payee Branch
			WebElement payeeBranch = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[4]/div[1]/div[2]"));

			action.moveToElement(payeeBranch);
			action.sendKeys(Keys.ENTER);
			action.perform();

			JavascriptExecutor jsex1 = (JavascriptExecutor)dr;
			jsex1.executeScript("scroll(0, 2000)");

			//Enter Bank
			lgn.locateElementByXPath("//*[@id=\'react-select-5--value\']", LoadProperties.dWPayeeBank);

			Thread.sleep(1000);

			//Select Bank
			WebElement bank = dr.findElement(By.xpath("//*[@id=\'root\']/div/div[2]/div[2]/div[2]/form/div[5]/div/div[2]/div/div[1]/div[2]"));

			action.moveToElement(bank);
			action.sendKeys(Keys.ENTER);
			action.perform();

			Thread.sleep(1000);

			//Enter Invoice Number
			invoiceNumber.sendKeys(LoadProperties.dWInvoiceNo);

			//Enter Purchase Order 
			purOrder.sendKeys(LoadProperties.dWPurchaseOdr);

			//Submit the invoice
			invSubmit.click();

			Thread.sleep(2000);	

			String expected = LoadProperties.url+"/invoice/list";
			boolean actual = dr.getCurrentUrl().equals(expected);

			if(actual){
				getToasterMsg = toasterMsg.getText();
				log.printLogsToFile(testCase +getToasterMsg);
				log.printLogsToFile("********** " + testCase + "Finished ***********");
			}
			else{
				getToasterMsg = toasterMsg.getText();
				co.getScreenshot(dr);
				log.printLogsToFile(testCase +getToasterMsg);
				log.printLogsToFile("********** " + testCase + "Finished ***********");
				Assert.assertEquals(actual,expected,"Generate District Wise Invoice----->Invoice generation failed!");
			}
		}
	}
	@AfterMethod
	public void afterMethod(){
		dr.quit();
	}
}
